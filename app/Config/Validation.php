<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;
use CodeIgniter\Validation\CustomRules;

class Validation
{
    //--------------------------------------------------------------------
    // Setup
    //--------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var string[]
     */
    public $ruleSets = [
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
        CustomRules::class,
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */
    public $templates = [
        'list'   => 'CodeIgniter\Validation\Views\list',
        'single' => 'CodeIgniter\Validation\Views\single',
    ];
    
    public $SolicitudesValidaciones = [
        
        'nif' => 'required | nif_correcto',
        'nombre' => 'required',
        'apellido1' => 'required',
        'apellido2' => 'required',
        'anyo' => 'required | greater_than_equal_to[2017] | less_than[2022]',
        'email1' => 'required | matches',
        'email2' => 'required | matches',
    ];

    //--------------------------------------------------------------------
    // Rules
    //--------------------------------------------------------------------
}

class CustomRules{
    public function nif_correcto($str) {
        $str = trim($str);
        $str = str_replace('-', '', $str);
        $str = str_ireplace(' ', '', $str);
        
        if (!preg_match("/^[0-9]{7,8}[a-zA-Z]{1}$/" , $str) )
        {
            return FALSE;
        }
        else {
            
            $n = substr($str, 0, -1);
            $letter = substr($str, -1);
            $letter2 = substr("TRWAGMYFPDXBNJZSQVHLCKE", $N%23, 1);
            if(strtolower($letter) != strtolower($letter2)){
                return FALSE;
            }
        }
        return TRUE;
    }
}
