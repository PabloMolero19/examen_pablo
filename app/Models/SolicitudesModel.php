<?php
    namespace App\Models;

    use CodeIgniter\Model;

    class SolicitudesModel extends Model {
    protected $table = 'pau';
    protected $allowedFields = ['pau.nif', 'pau.nombre', 'pau.apellido1', 'pau.apellido2','pau.email', 'tipo_tasa',
                                'ciclos.nombre', 'ciclos.familia',];
}
