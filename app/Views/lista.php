<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="icon" href="<?= base_url('fotos/5.ico') ?>" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap5.css"/>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap5.js"></script>
        <title>ALUMNOS DEL CENTRO</title>
        <script>
            $(document).ready(function () {
                $('#tabla').dataTable({

                    "language": {

                        "info": "",
                        "lengthMenu": "Registros por página _MENU_",
                        "paginate": {

                            "next": "Siguiente",
                            "previous": "Anterior"
                        },
                        "search": "Busca un alumno: ",
                        "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    }
                });
            });
        </script>
    </head>

    <style>

        body {

            background-color:#fdc963;
        }

        th {

            background-color:white;
            text-align: center;
        }

        tr {

            background-color:white;
        }

        div {

            margin:auto;
        }

    </style>
    <body>

        <h2 style="text-align: center;">LISTA DE LOS ALUMNOS DEL CENTRO</h2>
        <div class="container">
            <a href="http://localhost:8080/codeigniter/index.php/formularioController/" target="_blank"><button class="btn btn-success">
                    Añadir Alumnos
                </button></a> 
            <table class="table table-hover table-bordered" id="tabla">
                <thead>
                <th id="holaa">NIA</th>   
                <th id="holaa">Nombre</th>
                <th class="th_class">Apellido 1</th>
                <th class="th_class">Apellido 2</th>
                <th class="th_class">Email</th>
                <th class="th_class">Foto</th>
                <th class="th_class">Acciones</th>
                </thead>
                <tbody>
                    <?php foreach ($alumnos as $alumno): ?>
                    <p>
                    <tr>
                        <td class="bottom"><?= $alumno['NIA'] ?></td>
                        <td class="bottom"><?= $alumno['nombre'] ?></td>
                        <td class="bottom"> <?= $alumno['apellido1'] ?></td>
                        <td class="bottom"> <?= $alumno['apellido2'] ?></td>
                        <td class="bottom"> <?= $alumno['email'] ?></td>
                        <td class="bottom"> <img src= "<?= base_url('FotosAlu/' . sprintf('%06s', $alumno['id'])) ?>.jpg" width="50px" style="margin-left:16%;"> </td>
                        <td class="bottom"><a href="http://localhost:8080/codeigniter/index.php/formularioBorrarController/eliminar/<?= $alumno ['NIA']; ?>" onclick="return confirm('¿Estás seguro? Vas a eliminar al alumno');"><button type="submit" class="btn btn-danger" style="margin-left:16%; width:100px;">Borrar</button><br><br><a href="http://localhost:8080/codeigniter/index.php/formularioController/editar/<?= $alumno ['NIA']; ?>" target="_blank"><button type="submit" class="btn btn-info" style="margin-left:16%;">Actualizar</button></a></a></td>
                    </tr>
                    </p>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </body>
</html>