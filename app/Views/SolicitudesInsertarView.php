<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>FORMULARIO</title>
    </head>

    <style>

        body {

            background-color:#fdc963;
        }

        input:hover {

            background-color: paleturquoise;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h2 style="text-align: center;">DATOS DEL SOLICITANTE</h2>
        <div class="container" id="alineacion">
            <form method="post" action="<?= base_url("index.php/SolicitudesController/insertar") ?>">
                <?= $validation->listErrors() ?>
                <?= form_open('SolicitudesController') ?>
                <h3>Inserta el NIF</h3>
                <input type="text" name="nif" value="" size="10" placeholder="Inserta el NIF" pattern="[0-9]{7,8}[a-zA-Z]{1}"
                <div class="form-group" style="width:450px;">
                    &nbsp;<label name="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Insertar Nombre">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    &nbsp;<label name="apellido1">Primer Apellido</label>
                    <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="Insertar Primer Apellido">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    &nbsp;<label name="apellido2">Segundo Apellido</label>
                    <input type="text" class="form-control" id="apellido2" name="apellido2" placeholder="Insertar Segundo Apellido">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    &nbsp;<label name="nacimiento">AÑO FIN DE ESTUDIOS</label>
                    <abbr title="Pon solo el año de finalización"><input type="text" class="form-control" id="nacimiento" name="anyo" value="2021"></abbr>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    &nbsp;<label name="email">Dirección de mail</label>
                    <input type="text" class="form-control" id="email" name="email1" placeholder="Insertar tu email">
                    <i><b>Vuelve a insertarlo por seguridad</b></i><br>
                    <input type="text" class="form-control" id="email" name="email2" placeholder="Insertar tu email">
                </div> <br>
                    
                <!-- las validaciones están en App/Config/validation.php y en app/validation/ -->

                <!-- form_fieldeset -->
                <div class="custom-control custom-radio custom-control-inline" style="width:450px;">
                    <!--<= form_fieldset('Elige tu nivel de ingles') ?>

                    <= form_label('ordinaria', 'tipo_tasa', ['class' => 'control-label']) ?>
                    <= form_radio('tipo_tasa', '1', FALSE, ['class' => 'custom-radio', 'id' => '1']) ?>

                    <= form_label('semigratuita', 'tipo_tasa', ['class' => 'control-label']) ?>
                    <= form_radio('tipo_tasa', '2', FALSE, ['class' => 'custom-radio', 'id' => '2']) ?>

                    <= form_label('gratuita', 'tipo_tasa', ['class' => 'control-label']) ?>
                    <= form_radio('tipo_tasa', '3', FALSE, ['class' => 'custom-radio', 'id' => '3']) ?>

                    <= form_fieldset_close() ?> sé que falta en cada bloque de impresión el ?, pero si no, no me deja comentar--> 
                </div>

                <!-- form_dropdown TABLA CICLO -->
                <br><div class="form-group" style="width:450px;">
                    <!--<= form_fieldset('Selecciona el nombre del grado al que quieres solicitar') ?>
                    <= form_label('nombre', 'nombre', ['class' => 'control-label']) ?>
                    <=
                    form_dropdown('nombre', ['441104' => 'CFGS Administración y Finanzas',
                        '449104' => 'CFGS Comercio Internacional',
                        '472103' => 'CFGM Gestión Administrativa',
                        '481104' => 'CFGS Higiene Bucodental',
                        '483104' => 'CFGS Prótesis Dentales',
                        '707103' => 'CFGM Sistemas Microinformáticos y Redes',
                        '710103' => 'CFGM Farmacia y Parafarmacia',
                        '829104' => 'CFGS Administración de Sistemas Informáticos en Red',
                        '845104' => 'CFGS Desarrollo de Aplicaciones Web',
                        '899104' => 'CFGS Gestión de Ventas y Espacios Comerciales',
                        '906104' => 'CFGS Asistencia a la Dirección',
                        '925103' => 'CFGM Actividades Comerciales',
                        '950104' => 'CFGS Ortopŕotesis y Productos de Apoyo',
                        '975104' => 'CFGS Documentación y Administración Sanitarias',
                        '976104' => 'CFGS Imagen para el Diagnóstico y Medicina Nuclear',
                        '977104' => 'CFGS Laboratorio Clínico y Biomédico',
                        '978104' => 'CFGS Radioterapia y Dosimetría',],
                            ['class' => 'custom-select', 'id' => 'nombre'])
                    ?>
                    <= form_fieldset_close() ?>sé que falta en cada bloque de impresión el ?, pero si no, no me deja comentar--> 
                </div>

                <button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>


