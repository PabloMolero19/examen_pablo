<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap5.css"/>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap5.js"></script>
        <script>
            $(document).ready(function () {
                $('#tabla').dataTable({

                    "language": {

                        "info": "",
                        "lengthMenu": "Registros por página _MENU_",
                        "paginate": {

                            "next": "Siguiente",
                            "previous": "Anterior"
                        },
                        "search": "Busca un solicitante: ",
                        "infoFiltered": "(Filtrado de un total de _MAX_ registros)",
                    }
                });
            });
        </script>
        <title>Examen 1 ev</title>
        
    </head>

    <style>

        body {

            background-color:#fdc963;
        }

        th {

            background-color:white;
            text-align: center;
        }

        tr {

            background-color:white;
        }

        div {

            margin:auto;
        }

    </style>
      
    </style>
    <body>
        <h2 style="text-align: center;">LISTADO DE LAS SOLICITUDES</h2>
        <div class="container">
            <a href="http://localhost:8081/codeigniter/index.php/SolicitudesController/insertar" target="_blank"><button class="btn btn-success">
                    Añadir Solicitantes
                </button></a>
            <table class="table table-hover table-bordered" id="tabla">
                <thead>
                <th id="holaa">NIE / NIF</th>   
                <th id="holaa">Solicitante</th>
                <th class="th_class">email</th>
                <th class="th_class">ciclo</th>
                <th class="th_class">matrícula</th>
                <th class="th_class">Acciones</th>
                </thead>
                <tbody>
                    <!-- si quito el foreach me funciona, si no, me da error también sustituir por menor que?php foreach ($solicitantes as $solicitante): ?>-->
                    <tr> <!-- Si quito los bloques de impresión contenidos en la tabla, me funciona perfectamente,
                            pero cuando los pongo me da eror -->
                            <td id="holaa">prueba<!--sustituir por menor que?= solicitante['nif'] ?>--></td>   
                            <td id="holaa">prueba</td>  
                            <td class="th_class">prueba<!--sustituir por menor que?= solicitante['email'] ?>--></td>  
                            <td class="th_class">prueba<!--sustituir por menor que?= solicitante['ciclo'] ?>--></td>  
                            <td class="th_class">prueba</td>  
                            <td class="bottom"><a href="http://localhost:8080/codeigniter/index.php/SolicitudesController/eliminar/" onclick="return confirm('¿Estás seguro? Vas a eliminar al solicitante y no podrás recuperarlo');"><button type="submit" class="btn btn-danger" style="margin-left:25%; width:100px;">Borrar</button>
                        </tr>
                     <!--sustituir por menor que?php endforeach; ?>-->
                </tbody>
            </table>
        </div>

    </body>
</html>